using System;
using System.Collections.Generic;
using System.Reflection;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Attributes;
using UnityEditor;
using UnityEngine;

namespace AttaboysGames.Audio.Editor.Package.Scripts.Editor
{
	[InitializeOnLoad]
	[CustomPropertyDrawer(typeof(AudioAttribute))]
	public class AudioAttributePropertyDrawer : PropertyDrawer
	{
		private static readonly List<string> values;

		static AudioAttributePropertyDrawer()
		{
			values = new List<string>();
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			var suitStructs = 0;

			foreach (var assembly in assemblies)
			{
				var types = assembly.GetTypes();

				foreach (var type in types)
				{
					if (!IsStruct(type))
						continue;

					var customAttribute = type.GetCustomAttribute(typeof(AudioStructAttribute), false);

					if (customAttribute == null)
						continue;

					suitStructs++;
					var fieldInfos = type.GetFields();

					foreach (var info in fieldInfos)
					{
						if (info.IsSpecialName)
							continue;

						values.Add(info.Name);
					}
				}
			}
			
			if(suitStructs <= 1)
				return;
			
			Debug.LogWarning($"{nameof(AudioAttributePropertyDrawer)} You have more than 1 structure with {nameof(AudioStructAttribute)}. You may have only one such structure.");
			values = new List<string>();
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var value = property.intValue;
			var commonWidth = position.width;
			var oneThirdWidth = (commonWidth) / 3;
			position.width = oneThirdWidth;
			EditorGUI.LabelField(position, "Type");
			position.x += position.width;
			position.width = oneThirdWidth * 2;
			EditorGUI.BeginChangeCheck();
			var selectedValue = EditorGUI.Popup(position, value, values.ToArray());
			
			if (EditorGUI.EndChangeCheck())
				property.intValue = selectedValue;
		}

		private static bool IsStruct(Type type) => type.IsValueType && !type.IsPrimitive && !type.IsEnum;
	}
}