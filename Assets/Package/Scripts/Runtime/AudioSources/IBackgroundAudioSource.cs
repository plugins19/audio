using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources
{
	public interface IBackgroundAudioSource
	{
		bool IsPlaying { get; }
		
		bool IsFinished();
		
		void PlayClip(AudioClip clip);

		void SetVolume(float volume);

		void Play();

		void Pause();
	}
}