using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources
{
	public interface ISingleAudioSource
	{
		bool IsFinished { get; }
		
		void PlayClip(AudioClip clip, float volume);

		void SetActive(bool isActive);
	}
}