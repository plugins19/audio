using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources.Impls
{
	public class BackgroundAudioSource : MonoBehaviour, IBackgroundAudioSource
	{
		[SerializeField] private AudioSource audioSource;

		public bool IsPlaying => audioSource.isPlaying;

		public bool IsFinished()
		{
			var clipLength = audioSource.clip.length;
			var currentTime = audioSource.time;
			return clipLength - currentTime < 0.01f;
		}

		public void PlayClip(AudioClip clip)
		{
			audioSource.clip = clip;
			audioSource.Play();
		}

		public void SetVolume(float volume) => audioSource.volume = volume;

		public void Play() => audioSource.Play();

		public void Pause() => audioSource.Pause();
	}
}