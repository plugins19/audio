using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources.Impls
{
	public class SingleAudioSource : MonoBehaviour, ISingleAudioSource
	{
		[SerializeField] private AudioSource audioSource;

		public bool IsFinished => !audioSource.isPlaying;

		public void PlayClip(AudioClip clip, float volume)
		{
			audioSource.clip = clip;
			audioSource.volume = volume;
			audioSource.Play();
		}

		public void SetActive(bool isActive) => gameObject.SetActive(isActive);
	}
}