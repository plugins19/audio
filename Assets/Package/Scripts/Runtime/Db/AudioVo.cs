using System;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Attributes;
using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Db
{
	[Serializable]
	public struct AudioVo
	{
		[Audio] public int id; 
		public AudioClip clip;
		public float volume;
	}
}