using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources.Impls;
using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Db
{
	public interface IAudioDatabase
	{
		SingleAudioSource SingleAudioSourcePrefab { get; }
		
		int BackgroundClipsCount { get; }
		
		float BackgroundVolume { get; }
	
		AudioClip GetBackgroundByIndex(int index);
	
		AudioVo Get(int audioId);
	}
}