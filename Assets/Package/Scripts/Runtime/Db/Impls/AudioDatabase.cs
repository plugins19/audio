using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources.Impls;
using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Db.Impls
{
	[CreateAssetMenu(menuName = "Databases/Attaboys/" + nameof(AudioDatabase), fileName = nameof(AudioDatabase))]
	public class AudioDatabase : ScriptableObject, IAudioDatabase
	{
		[SerializeField] private SingleAudioSource singleAudioSource;
		[SerializeField] private AudioClip[] backgroundClips;
		[SerializeField] private float backgroundVolume = 1f;

		[KeyValueAttribute.Runtime.Package.Runtime.KeyValue("clip")][SerializeField] private AudioVo[] vos;

		public SingleAudioSource SingleAudioSourcePrefab => singleAudioSource;

		public int BackgroundClipsCount => backgroundClips.Length;

		public float BackgroundVolume => backgroundVolume;

		public AudioClip GetBackgroundByIndex(int index) => backgroundClips[index];
		
		public AudioVo Get(int audioId)
		{
			foreach (var vo in vos)
			{
				if (vo.id != audioId)
					continue;

				return vo;
			}

			Debug.LogWarning(
				$"{nameof(AudioDatabase)} You have tried to play audio clip {audioId}, but it doesn't exist in database");
			return default;
		}
	}
}