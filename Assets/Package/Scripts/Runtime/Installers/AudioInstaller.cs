using AttaboysGames.Package.Runtime;
using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources.Impls;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Db.Impls;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Pool.Impls;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Services.Impls;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Storages.Impls;
using UnityEngine;
using Zenject;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Installers
{
	[CreateAssetMenu(menuName = "Installers/Attaboys/" + nameof(AudioInstaller), fileName = nameof(AudioInstaller))]
	public class AudioInstaller : ScriptableObjectInstaller
	{
		[SerializeField] private BackgroundAudioSource backgroundAudioSourcePrefab;
		[SerializeField] private AudioDatabase audioDatabase;

		public override void InstallBindings()
		{
			Container.BindDatabase(audioDatabase);
			var backgroundAudioSourceInstance =
				Container.InstantiatePrefabForComponent<BackgroundAudioSource>(backgroundAudioSourcePrefab);
			Container.BindInterfacesTo<BackgroundAudioSource>().FromInstance(backgroundAudioSourceInstance).AsSingle().WhenInjectedInto<BackgroundAudioService>();
			Container.BindInterfacesTo<BackgroundAudioService>().AsSingle();
			Container.BindInterfacesTo<AudioService>().AsSingle();
			Container.BindInterfacesTo<SingleAudioSourcePool>().AsSingle();
			Container.BindInterfacesTo<AudioStorage>().AsSingle();
		}
	}
}