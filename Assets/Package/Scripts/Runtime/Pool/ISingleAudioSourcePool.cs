using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Pool
{
	public interface ISingleAudioSourcePool
	{
		ISingleAudioSource Get();

		void ToPool(ISingleAudioSource source);
	}
}