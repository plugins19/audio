using System.Collections.Generic;
using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources;
using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources.Impls;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Db;
using Zenject;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Pool.Impls
{
	public class SingleAudioSourcePool : ISingleAudioSourcePool
	{
		private readonly DiContainer _container;
		private readonly IAudioDatabase _audioDatabase;
		
		private readonly List<ISingleAudioSource> _sources = new();

		public SingleAudioSourcePool(
			DiContainer container,
			IAudioDatabase audioDatabase
		)
		{
			_container = container;
			_audioDatabase = audioDatabase;
		}

		public ISingleAudioSource Get()
		{
			if (_sources.Count == 0)
			{
				var audioSourcePrefab = _audioDatabase.SingleAudioSourcePrefab;
				var audioSourceInstance = _container.InstantiatePrefabForComponent<SingleAudioSource>(audioSourcePrefab);
				return audioSourceInstance;
			}

			var singleAudioSource = _sources[0];
			_sources.RemoveAt(0);
			singleAudioSource.SetActive(true);
			return singleAudioSource;
		}

		public void ToPool(ISingleAudioSource source)
		{
			source.SetActive(false);
			_sources.Add(source);
		}
	}
}