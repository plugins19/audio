namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Services
{
	public interface IAudioService
	{
		void Play(int audioId);
	}
}