using System;
using System.Collections.Generic;
using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Db;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Pool;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Storages;
using UniRx;
using Zenject;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Services.Impls
{
	public class AudioService : IAudioService, IInitializable, ITickable, IDisposable
	{
		private readonly IAudioDatabase _audioDatabase;
		private readonly ISingleAudioSourcePool _pool;
		private readonly IAudioStorage _audioStorage;
		private readonly List<ISingleAudioSource> _sources = new();
		
		private IDisposable _disposable = Disposable.Empty;

		public AudioService(
			IAudioDatabase audioDatabase,
			ISingleAudioSourcePool pool,
			IAudioStorage audioStorage
		)
		{
			_audioDatabase = audioDatabase;
			_pool = pool;
			_audioStorage = audioStorage;
		}

		public void Play(int audioId)
		{
			if(!_audioStorage.IsSound.Value)
				return;
			
			var vo = _audioDatabase.Get(audioId);
			var singleAudioSource = _pool.Get();
			singleAudioSource.PlayClip(vo.clip, vo.volume);
			_sources.Add(singleAudioSource);
		}

		public void Initialize()
		{
			_disposable = _audioStorage.IsSound
				.Subscribe(OnIsSoundChanged);
		}

		public void Tick()
		{
			for (var index = 0; index < _sources.Count; index++)
			{
				var source = _sources[index];
				
				if (!source.IsFinished)
					continue;

				_sources.RemoveAt(index);
				_pool.ToPool(source);
			}
		}

		public void Dispose() => _disposable.Dispose();

		private void OnIsSoundChanged(bool value)
		{
			if(value)
				return;

			for (var index = 0; index < _sources.Count; index++)
			{
				var source = _sources[index];
				_sources.RemoveAt(index);
				_pool.ToPool(source);
			}
		}
	}
}