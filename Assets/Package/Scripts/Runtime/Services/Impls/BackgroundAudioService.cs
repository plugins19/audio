using System;
using AttaboysGames.Package.Scripts.Runtime.Providers;
using AttaboysGames.Runtime.Package.Scripts.Runtime.AudioSources;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Db;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Db.Impls;
using AttaboysGames.Runtime.Package.Scripts.Runtime.Storages;
using AttaboysGames.YandexGameBuildPostProcessor.Runtime.Package.Runtime.Scripts.Storages;
using UniRx;
using UnityEngine;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Services.Impls
{
	public class BackgroundAudioService : IBackgroundAudioService, IDisposable
	{
		private readonly IAudioDatabase _audioDatabase;
		private readonly IBackgroundAudioSource _backgroundAudioSource;
		private readonly IRandomProvider _randomProvider;
		private readonly IAudioStorage _audioStorage;
		private readonly IApplicationTabActivityStorage _applicationTabActivityStorage;

		private IDisposable _disposable = Disposable.Empty;
		private IDisposable _applicationTabActiveDisposable = Disposable.Empty;
		private bool _wasAudioActiveBeforeAppStateChanged;

		public BackgroundAudioService(
			IAudioDatabase audioDatabase,
			IBackgroundAudioSource backgroundAudioSource,
			IRandomProvider randomProvider,
			IAudioStorage audioStorage,
			IApplicationTabActivityStorage applicationTabActivityStorage
		)
		{
			_audioDatabase = audioDatabase;
			_backgroundAudioSource = backgroundAudioSource;
			_randomProvider = randomProvider;
			_audioStorage = audioStorage;
			_applicationTabActivityStorage = applicationTabActivityStorage;
		}

		public void Initialize()
		{
			PlayNewClip();
			_backgroundAudioSource.SetVolume(_audioDatabase.BackgroundVolume);

			_disposable = _audioStorage.IsBackground
				.Subscribe(OnIsBackgroundChanged);

			_applicationTabActiveDisposable = _applicationTabActivityStorage.IsTabActive
				.Subscribe(OnApplicationTabActiveStateChanged);
		}

		public void Tick()
		{
			if (!_backgroundAudioSource.IsFinished())
				return;

			PlayNewClip();
		}

		public void Dispose()
		{
			_disposable.Dispose();
			_applicationTabActiveDisposable.Dispose();
		}

		private void OnIsBackgroundChanged(bool value)
		{
			if (value)
				_backgroundAudioSource.Play();
			else
				_backgroundAudioSource.Pause();
		}

		private void Play()
		{
			if (!_audioStorage.IsBackground.Value)
				return;

			_backgroundAudioSource.Play();
		}

		private void Pause() => _backgroundAudioSource.Pause();

		private void OnApplicationTabActiveStateChanged(bool isActive)
		{
			if (!isActive)
			{
				_wasAudioActiveBeforeAppStateChanged = _backgroundAudioSource.IsPlaying;
				Pause();
				return;
			}
			
			if(!_wasAudioActiveBeforeAppStateChanged)
				return;
			
			Play();
		}

		private void PlayNewClip()
		{
			var backgroundClipsCount = _audioDatabase.BackgroundClipsCount;

			if (backgroundClipsCount == 0)
			{
				Debug.LogWarning($"{nameof(BackgroundAudioService)} You haven't added background sounds int the {nameof(AudioDatabase)}");
				return;
			}

			var randomClipIndex = _randomProvider.Range(backgroundClipsCount);
			var backgroundByIndex = _audioDatabase.GetBackgroundByIndex(randomClipIndex);
			_backgroundAudioSource.PlayClip(backgroundByIndex);
		}
	}
}