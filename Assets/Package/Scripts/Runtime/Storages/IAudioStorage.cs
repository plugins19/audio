using UniRx;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Storages
{
	public interface IAudioStorage
	{
		IReadOnlyReactiveProperty<bool> IsSound { get; }
		
		IReadOnlyReactiveProperty<bool> IsBackground { get; }

		void InverseIsBackground();

		void InverseIsSound();
	}
}