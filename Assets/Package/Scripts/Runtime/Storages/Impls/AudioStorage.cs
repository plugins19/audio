using UniRx;

namespace AttaboysGames.Runtime.Package.Scripts.Runtime.Storages.Impls
{
	public class AudioStorage : IAudioStorage
	{
		private readonly BoolReactiveProperty _isSound = new(true);
		public IReadOnlyReactiveProperty<bool> IsSound => _isSound;

		private readonly BoolReactiveProperty _isMusic = new(true);
		public IReadOnlyReactiveProperty<bool> IsBackground => _isMusic;

		public void InverseIsSound() => _isSound.Value = !_isSound.Value;

		public void InverseIsBackground() => _isMusic.Value = !_isMusic.Value;
	}
}