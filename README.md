Packages:
```
"com.dikiyhimik.zenject": "https://gitlab.com/plugins19/zenject.git#upm",
"com.neuecc.unirx": "https://github.com/neuecc/UniRx.git?path=Assets/Plugins/UniRx/Scripts",
"com.attaboysgames.unityrandom": "https://gitlab.com/plugins19/unityrandom.git#upm",
"com.attaboysgames.zenjectextensions": "https://gitlab.com/plugins19/zenjectextensions.git#upm",
"com.attaboysgames.keyvalue": "https://gitlab.com/plugins19/keyvalueattribute.git#upm",
"com.attaboysgames.platformtype": "https://gitlab.com/plugins19/platformtype.git#upm",
"com.attaboysgames.yandexgamebuildpostprocessor" : "https://gitlab.com/plugins19/yandexgamesbuildpostprocessor.git#upm",
"com.attaboysgames.audio" : "https://gitlab.com/plugins19/audio.git#upm",

```

Instructions:
 - create struct with attribute "AudioStructAttribute" and add to it public readonly ushort field with audio names. 
The names of this fields will be used in database.
 - Create Audio database scriptable object, right click -> Databases -> Attaboys -> AudioDatabase 
and single audio source prefab from prefab folder from this package
 - Fill this db by single audio source settings and background sounds
 - Create Audio installer, right click -> Installers -> Attaboys -> AudioInstaller
 - Fill it by background audio source form prefabs folder from this package and audio database
 - Add this installer to project context

Scripts:
- For enabling or disabling background or single sounds you should get IAudioStorage into your script and use Inverse methods
- For enabling single SFX you should get IAudioService and use Play(ushort value) method
